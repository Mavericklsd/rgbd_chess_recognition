Instructions:

- The first 4 projects are necessary, I included the viewer because I think 
it's useful to see the pointclouds if something goes wrong.

- After you use CMake to create all the projects, I recommend changing the
working directories (Project Properties -> Debugging -> Working Directory) 
of all of them to ../../ because then you won't need to keep moving the files
from one folder to the other.

How to create templates:

- Use the grabber project to grab .pcd files from the objects.

	-The camera must not be seeing anything to the sides of the object 
	 you are training. 

	-Type 's' to save the frame as a pcd file. Each file will be saved as
	 frameX.pcd, with the first frame being frame0.pcd by default. 

	-If you want to change the index of the first file, you can pass the
	 argument -init (Y) to the program, and the first frame will be
	 saved as frame(Y).pcd

	-You can type 'p (number)' in the command prompt to draw planes on 
	 the visualizer, in order to know the z values of the clipping planes 
	 for the next step.

- Use the train_template project to create template files.

	-There are 5 necessary arguments to run this program:

	1. -start S: The index of the first pcd file (frameS.pcd) that will
	   be read.

	2. -finish F: The index of the last pcd file that will be read.

	3. -min_depth z_min: The z of the near clipping plane (usually can 
	   be 0)

	4. -max_depth z_max: The z of the far clipping plane (you shoud use
	   the p (number) thing I mentioned above in the grabber to find out 
	   this value.

	5. -no_floor n: 1 if the floor was not detected by the camera when
	   saving the pcd file, and 0 if the floor was detected.

- Use the concatenateTemplates to join all templates of a single object in a 
single template file.

	-You'll need to pass each .sqmmt file name as argument to the program,
	 and -out outputname (without .sqmmt).

	-If later you train more templates of the same object, you can then 
	 join these templates with the original one. For example, if you first
	 trained 5 templates of the mouse object, and used 
	 "concatenate_templates frame0_template.sqmmt (..) 
	  frame4_template.sqmmt -out mouse" to create the mouse.sqmmt file,
	 and then you trained 2 more template files of mouse. Now you can use
	 "concatenate_templates frame0_template.sqmmt 
	  frame1_template.sqmmt mouse.sqmmt -out mouse2" to create the new 
	 template.

- Use the match_template to detect the objects with the camera.

	-You need to pass the names of the object template files as arguments
	 to this program.

	-The name displayed on screen above the detection is the template file
	 name. For example, if you used the file MousE.sqmmt as argument, when
	 an object is detected, MousE will be written above the detection.