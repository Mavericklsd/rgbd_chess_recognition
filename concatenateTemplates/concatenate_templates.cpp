/* This simply takes some template files as input and saves everything as
 * one template file. 




 */

#include <math.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/console/time.h>

#include <pcl/PointIndices.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/recognition/linemod.h>
#include <pcl/recognition/color_gradient_modality.h>
#include <pcl/recognition/surface_normal_modality.h>

using namespace pcl;
using namespace pcl::io;
using namespace pcl::console;

typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloudXYZRGBA;

void
printHelp (int, char **argv)
{
  print_error ("Syntax is: %s input1.sqmmt input2.sqmmt input3.sqmmt (etc.) -out output\n", argv[0]);
  print_info ("output = the name of the output file (without .sqmmt\n");
}

int
main (int argc, char** argv)
{
  // If no arguments are given, print the help text
  if (argc == 1)
  {
    printHelp (argc, argv);
    return (-1);
  }

  // Parse the command line arguments for .sqmmt files
  std::vector<int> t_file_indices;
  t_file_indices = parse_file_extension_argument (argc, argv, ".sqmmt");
  if (t_file_indices.empty ())
  {
    print_error ("Need at least one input sqmmt file.\n");
    return (-1);
  }

  std::string output_name;
  parse_argument (argc, argv, "-out", output_name);

  std::vector<std::string> template_names;

  for (size_t i = 0; i < t_file_indices.size (); ++i){
//	printf("%d %d\n", i, t_file_indices.size());
	template_names.push_back(argv[t_file_indices[i]]);
  }

  pcl::LINEMOD linemod;

  linemod.loadTemplates (template_names);

  output_name.append(".sqmmt");

  linemod.saveTemplates(output_name.c_str());

    std::cout << output_name << std::endl;
}