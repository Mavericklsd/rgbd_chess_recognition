/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2011, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Suat Gedikli (gedikli@willowgarage.com)
 */


/* I used some code from the pcl openni_grabber_example and some from this
 * tutorial: http://pointclouds.org/documentation/tutorials/openni_grabber.php
 */

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/common/time.h>
#include <pcl/console/parse.h>
#include <pcl/visualization/cloud_viewer.h>

//global variable to store the z value of the plane
float zplane;

//draws a plane perpendicular to the camera's central ray at z = zplane
void drawPlane (pcl::visualization::PCLVisualizer& viewer)
{
	viewer.removeShape ("plane", 0);
	pcl::ModelCoefficients plane_coeff;
	plane_coeff.values.resize (4);
	plane_coeff.values[0] = 0.0;
	plane_coeff.values[1] = 0.0;
	plane_coeff.values[2] = 1.0;
	plane_coeff.values[3] = -zplane;
	viewer.addPlane(plane_coeff);
}

class SimpleOpenNIProcessor
{
  public:
    bool save;
	int framecounter;	// this variable is used to know which is the number in the name the file will be saved at
						// it usually strarts at 0, to change it, pass -init (number) as argument
    openni_wrapper::OpenNIDevice::DepthMode mode;	// this was present in the example and I didn't change it
	pcl::visualization::CloudViewer viewer;
    SimpleOpenNIProcessor (int init, openni_wrapper::OpenNIDevice::DepthMode depth_mode = openni_wrapper::OpenNIDevice::OpenNI_12_bit_depth) : mode (depth_mode), framecounter(init), viewer ("PCL OpenNI Viewer") {}

	//cloud_cb_ is the callback function for saving frames
    void 
    cloud_cb_ (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr &cloud)
    {
      if (save)
      {
        std::stringstream ss;
        ss << "frame" << framecounter << ".pcd";
		framecounter++;
        pcl::PCDWriter w;
        w.writeBinaryCompressed (ss.str (), *cloud);
        std::cout << "wrote point cloud to file " << ss.str () << std::endl;
		save = !save;
      }
    }

	//show_cb_ is the callback function for showing frames
	void show_cb_ (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr &cloud)
    {
       if (!viewer.wasStopped())
         viewer.showCloud (cloud);
    }

    void 
    run ()
    {
      save = false;

      // create a new grabber for OpenNI devices
      pcl::OpenNIGrabber interface;

      // Set the depth output format
      interface.getDevice ()->setDepthOutputFormat (mode);

	  //first we bind the saving function
      boost::function<void (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr&)> f =
        boost::bind (&SimpleOpenNIProcessor::cloud_cb_, this, _1);
      boost::signals2::connection c = interface.registerCallback (f);

	  //then we bind the drawing function
      boost::function<void (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr&)> f2 =
        boost::bind (&SimpleOpenNIProcessor::show_cb_, this, _1);
      boost::signals2::connection c2 = interface.registerCallback (f2);

      // start receiving point clouds
      interface.start ();

	  //handling the input
      char key;
      do
      {
        key = static_cast<char> (getchar ());
        switch (key)
        {
          case ' ':
            if (interface.isRunning ())
              interface.stop ();
            else
              interface.start ();
          case 's':
            save = !save;
		  case 'p':
			std::cin >> zplane;
			viewer.runOnVisualizationThreadOnce (drawPlane);
        }
      } while (key != 27 && key != 'q' && key != 'Q');

      // stop the grabber
      interface.stop ();
    }
};

int
main (int argc, char **argv)
{
  std::cout << "<Esc>, \'q\', \'Q\': quit the program" << std::endl;
  std::cout << "\' \': pause" << std::endl;
  std::cout << "\'s\': save" << std::endl;
  std::cout << "\'p (z)\': draw plane with z = (z)" << std::endl;

  int mode = openni_wrapper::OpenNIDevice::OpenNI_12_bit_depth;
  pcl::console::parse_argument (argc, argv, "-mode", mode);
  int init = 0;
  pcl::console::parse_argument (argc, argv, "-init", init);

  SimpleOpenNIProcessor v (init, static_cast<openni_wrapper::OpenNIDevice::DepthMode> (mode));
  v.run ();
  return (0);
}
