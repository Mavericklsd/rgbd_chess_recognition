/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2011, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id$
 *
 */

/* This program was based on the pcl_match_linemod_template example.
 * I changed it so that it can take more than one sqmmt file as input.
 * Also, it now takes the input from a camera, instead of only one .pcd file.
 * Instead of just outputting the position where the object is detected, I used
 * opencv to display the image with a rectangle around the detected objects.
 *
 * The input needs to have at least one .sqmmt file. All the templates for one object
 * need to be concatenated into one .sqmmt file, and the name of the file should be
 * the name of the object. For example, all the templates for a mouse object should be
 * saved in one mouse.sqmmt template file.
 */


#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/console/time.h>
#include <pcl/common/time.h>
#include <pcl/io/openni_grabber.h>

#include <pcl/recognition/linemod.h>
#include <pcl/recognition/color_gradient_modality.h>
#include <pcl/recognition/surface_normal_modality.h>
//#include <opencv/cv.h>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace pcl;
using namespace pcl::io;
using namespace pcl::console;
using namespace cv;

typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloudXYZRGBA;

void
printHelp (int, char **argv)
{
  print_error ("Syntax is: %s input_template1.sqmmt input_template2.sqmmt (...)\n", argv[0]);
}

class SimpleOpenNIProcessor
{
public:
	pcl::LINEMOD linemod;
	std::vector<std::string> detection_names;
	std::vector<int> nr_templates;
	Mat *A;
	SimpleOpenNIProcessor (pcl::LINEMOD l, std::vector<std::string> d,	std::vector<int> n) : linemod(l), detection_names(d), nr_templates(n) {
		A = new Mat(480, 640, CV_8UC3);
	}

	// this function was not changed from the example. It preprocesses the input data and calls
	// linemod's matchTemplates.
	std::vector<pcl::LINEMODDetection>
	matchTemplates (const PointCloudXYZRGBA::ConstPtr & input, const pcl::LINEMOD & linemod)
	{
	  pcl::ColorGradientModality<pcl::PointXYZRGBA> color_grad_mod;
	  color_grad_mod.setInputCloud (input);
	  color_grad_mod.processInputData ();

	  pcl::SurfaceNormalModality<pcl::PointXYZRGBA> surface_norm_mod;
	  surface_norm_mod.setInputCloud (input);
	  surface_norm_mod.processInputData ();

	  std::vector<pcl::QuantizableModality*> modalities (2);
	  modalities[0] = &color_grad_mod;
	  modalities[1] = &surface_norm_mod;

	  std::vector<pcl::LINEMODDetection> detections;
	  linemod.matchTemplates (modalities, detections);

	  return (detections);
	}

	// instead of loading the one pcd file, now this function takes the clouds from 
	// the camera.
	void
	compute (const PointCloudXYZRGBA::ConstPtr & input)
	{		
	  // Match the templates to the provided image
	  std::vector<pcl::LINEMODDetection> detections = matchTemplates (input, linemod);
  
	  // Output the position and score of the best match for each template
	  int i = 0;
	  for (size_t j = 0; j < nr_templates.size (); ++j)
	  {
		  for(size_t k = 0; k < nr_templates[j]; k++){
			  const LINEMODDetection & d = detections[i];
			  i++;
		  }
	  }

	  // now the detection has already finished, all this last piece of code is just to 
	  // display the detections using opencv

	  // firstly I draw the original image
	  MatIterator_<Vec3b> it, end;
	  i = 0;
	  for( it = (*A).begin<Vec3b>(), end = (*A).end<Vec3b>(); it != end; ++it)
	  {
		const pcl::PointXYZRGBA & p = input->points[i++];
		(*it)[0] = p.b;
		(*it)[1] = p.g;
		(*it)[2] = p.r;
	  }

	  // now I compute the best detection for each object. If you want to detect 
	  // multiple similar objects with the same template, you will need to change
	  // something around here
	  i = 0;
	  for (size_t j = 0; j < nr_templates.size (); ++j)
	  {
		  int imax = 0;
		  float max = 0.0;
		  int i0 = i;
		  for (size_t k = 0; k < nr_templates[j]; k++){
			  const LINEMODDetection & d = detections[i];
			  if(d.score > max){
				  max = d.score;
				  imax = i;
			  }
			  i++;
		  }

		  // if the best detection has a score bigger than a threshold, I draw a rectangle with the size of the
		  // template, using the detection position as the upper left corner of the rectangle. I also draw the 
		  // name of the object above the rectangle
		  Scalar colour(0, 0, 0);
		  printf("object: %s - best match: %d - score: %f\n", detection_names[j], imax-i0, max);
		  if (max > 0.83){
			  const LINEMODDetection & d = detections[imax];
			  const pcl::SparseQuantizedMultiModTemplate & tmplt = linemod.getTemplate (imax);
			  int w = tmplt.region.width;
			  int h = tmplt.region.height;
			  rectangle(*A, Point(d.x,d.y), Point(d.x+w, d.y+h), colour, 2);
			  putText(*A, detection_names[j], Point(d.x, d.y-5), FONT_HERSHEY_SIMPLEX, 1.0, colour, 2);
		  }
	  }
	}
  
  void run ()
  {
    // create a new grabber for OpenNI devices
    pcl::Grabber* interface = new pcl::OpenNIGrabber();

    // make callback function from member function
    boost::function<void (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr&)> f =
      boost::bind (&SimpleOpenNIProcessor::compute, this, _1);

    // connect callback function for desired signal. In this case its a point cloud with color values
    boost::signals2::connection c = interface->registerCallback (f);

    // start receiving point clouds
    interface->start ();

    // wait until user quits program with Ctrl-C, displaying the image;
    while (true){
	  imshow("frame", *A);
	  cv::waitKey(10);
	}
    // stop the grabber
    interface->stop ();
  }
};


/* ---[ */
int
main (int argc, char** argv)
{
  // Load the specified templates
  std::vector<int> template_file_indices;
  template_file_indices = parse_file_extension_argument (argc, argv, ".sqmmt");
  if (template_file_indices.empty ())
  {
    print_error ("Need at least one input SQMMT file.\n");
    return (-1);
  }
  std::vector<std::string> templates_filenames;

  for (size_t i = 0; i < template_file_indices.size (); ++i)
	templates_filenames.push_back(argv[template_file_indices[i]]);

  pcl::LINEMOD linemod;
  // Load the templates from disk
  linemod.loadTemplates (templates_filenames);
  std::vector<std::string> detection_names;		// detection_names stores the name of each object (the part before the .sqmmt extension)
  std::vector<int> nr_templates;				// nr_templates stores the number of templates each object has
  for(size_t i=0; i < templates_filenames.size (); i++)
  {
	int index = templates_filenames[i].find_last_of(".");
	std::string name = templates_filenames[i].substr(0, index);
	std::ifstream file_stream;
	file_stream.open (templates_filenames[i].c_str (), std::ofstream::in | std::ofstream::binary);		
	int aux;
	// the first number of the template file is the number of templates in the file
	read (file_stream, aux);
	nr_templates.push_back(aux);
	detection_names.push_back(name);
	file_stream.close ();
  }

  SimpleOpenNIProcessor v(linemod, detection_names, nr_templates);
  v.run();
}

